Run in foreground
`docker run -it registry.gitlab.com/llamashoes/telnetstarwars-docker:latest`

Run in background
`docker run -dt registry.gitlab.com/llamashoes/telnetstarwars-docker:latest`
